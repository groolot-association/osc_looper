#ifndef EXCEPTIONS_HPP
#define EXCEPTIONS_HPP

#include <stdexcept>

class NotImplemented : public std::logic_error
{
 public:
  NotImplemented() : std::logic_error("not implemented yet") {};
  NotImplemented(std::string _reason) : std::logic_error(_reason) {};
  NotImplemented(NotImplemented &&) = delete;
  NotImplemented(const NotImplemented &) = delete;
};

#endif // EXCEPTIONS_HPP
