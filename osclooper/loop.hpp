#ifndef LOOP_HPP
#define LOOP_HPP

#include <chrono>
#include <thread>
#include <cstdint>
#include <map>

#include <osclooper/osc.hpp>
#include <osclooper/record.hpp>

#include <log4cxx/logger.h>
#include <log4cxx/logmanager.h>
#include <log4cxx/basicconfigurator.h>

class loop : public osc::Node
{
 public:
  explicit loop();
  ~loop();

  std::string get_name();
  static int control_handler(const char *path,
                             const char *types,
                             lo_arg **arguments,
                             int nbarguments,
                             void *,//message,
                             void *user_data);

  static log4cxx::LoggerPtr logger;

 private:
  std::multimap<double, record> messages;
};

#endif // LOOP_HPP
