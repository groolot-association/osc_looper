#ifndef LOOPMANAGER_HPP
#define LOOPMANAGER_HPP

#ifdef HAVE_CONFIG_H
  #include "config.h"
#endif

#include <osclooper/loop.hpp>

#include <string>
#include <vector>

#include <log4cxx/logger.h>
#include <log4cxx/logmanager.h>
#include <log4cxx/basicconfigurator.h>

class loopManager : public osc::Node
{
 public:
  /** \brief Base constructor
   *
   * This is to construct a loopManager.
   */
  explicit loopManager();

  /** \brief Destruct loopManager object
   *
   */
  ~loopManager();

  /** \brief Loops map accessor
   *
   * \return Loops map pointer
   */
  const std::vector<loop *> *get_loops();

  /** \brief Add loop to the manager
   */
  void add(std::chrono::microseconds length);

  /** \brief List registered loops
   */
  void list();

  /** \brief The logger for the whole class
   *
   * It's using \e liblog4cxx for the logging mechanism.
   */
  static log4cxx::LoggerPtr logger;

 private:

  /** \brief List of loops in the whole program
   *
   * Loop dictionary used to keep track of loops defined anywhere
   * in the developper program
   */
  std::vector<loop *> loops;

  /** \brief OSC control commands
   *
   * These commands are defined with their arguments. \c std::multimap
   * is used to allow similar keys.
   */
  std::multimap<std::string, std::string> control_commands =
  {
    {"/list", ""},
  };

};

#endif // LOOPMANAGER_HPP
