#include <osclooper/record.hpp>
#include <osclooper/exceptions.hpp>

#include <chrono>

using namespace std::literals::chrono_literals;

log4cxx::LoggerPtr record::logger = log4cxx::Logger::getLogger("osclooper.record");

record::record(lo::Message _message,
               lo::Address _address) :
  message(_message),
  address(_address)
{
  if(!log4cxx::LogManager::getLoggerRepository()->isConfigured())
  {
    log4cxx::BasicConfigurator::configure();
  }

  throw NotImplemented();
}

record::~record()
{}
