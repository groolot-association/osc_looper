#include <osclooper/engine.hpp>
#include <osclooper/exceptions.hpp>

#include <cppunit/extensions/HelperMacros.h>
#include <iostream>
#include <sstream>
#include <vector>
#include <stdexcept>
#include <chrono>
#include <thread>
#include <atomic>
#include <lo/lo.h>
#include <lo/lo_cpp.h>
using namespace std::literals::chrono_literals;

class engineTest : public CppUnit::TestFixture
{
  CPPUNIT_TEST_SUITE(engineTest);
  CPPUNIT_TEST(testDefaultConstructor);
  CPPUNIT_TEST(testNameConstructor);
  CPPUNIT_TEST(testNamePeriodConstructor);
  CPPUNIT_TEST(testNamePeriodOSCPortConstructor);
  CPPUNIT_TEST(testRunningThread);
  CPPUNIT_TEST(testRestartingThread);
  CPPUNIT_TEST(testAlreadyStartedThread);
  CPPUNIT_TEST(testStopThread);
  CPPUNIT_TEST(testConflictedNameEngine);
  CPPUNIT_TEST(testInvalidNameEngine);
  CPPUNIT_TEST(testConflictedOSCPortEngine);
  CPPUNIT_TEST(testGetLoopManager);
  CPPUNIT_TEST(testRemoteStart);
  CPPUNIT_TEST(testRemoteStop);
  CPPUNIT_TEST(testRemotePing);
  CPPUNIT_TEST(testStressRemotePing);
  CPPUNIT_TEST(test_set_name);
  CPPUNIT_TEST_SUITE_END();
 public:
  void setUp();
  void tearDown();
  void testDefaultConstructor();
  void testNameConstructor();
  void testNamePeriodConstructor();
  void testNamePeriodOSCPortConstructor();
  void testRunningThread();
  void testRestartingThread();
  void testAlreadyStartedThread();
  void testStopThread();
  void testConflictedNameEngine();
  void testInvalidNameEngine();
  void testConflictedOSCPortEngine();
  void testGetLoopManager();
  void testRemoteStart();
  void testRemoteStop();
  void testRemotePing();
  void testStressRemotePing();
  void test_set_name();
 private:
  engine *test_engine;
  std::ostringstream local;
  std::streambuf *cout_buff;
  std::chrono::milliseconds standard_engine_period;
  unsigned short int standard_osc_port;
  std::chrono::milliseconds standard_wait_time;
};

// Registers the fixture into the 'registry'
CPPUNIT_TEST_SUITE_REGISTRATION(engineTest);

void engineTest::setUp()
{
  cout_buff = std::cout.rdbuf();  ///> save pointer to std::cout buffer
  std::cout.rdbuf(local.rdbuf());  ///> set new local buffer to std::cout buffer
  standard_engine_period = 10ms;
  standard_osc_port = 9000;
  standard_wait_time = 10ms;
}

void engineTest::tearDown()
{
  std::cout.rdbuf(cout_buff);  ///> go back to old buffer
  delete test_engine;
}

void engineTest::testDefaultConstructor()
{
  test_engine = new engine();
  CPPUNIT_ASSERT("noName" == test_engine->get_name());
  CPPUNIT_ASSERT(1s == test_engine->get_period());
}

void engineTest::testNameConstructor()
{
  std::string _name = "testNameConstructor";
  test_engine = new engine(_name);
  CPPUNIT_ASSERT(_name == test_engine->get_name());
  CPPUNIT_ASSERT(1s == test_engine->get_period());
}

void engineTest::testNamePeriodConstructor()
{
  std::string _name = "testNamePeriodConstructor";
  std::chrono::microseconds _period = 100ms;
  test_engine = new engine(_name, _period);
  CPPUNIT_ASSERT(_name == test_engine->get_name());
  CPPUNIT_ASSERT(_period == test_engine->get_period());
}

void engineTest::testNamePeriodOSCPortConstructor()
{
  std::string _name = "testNamePeriodOSCPortConstructor";
  std::chrono::microseconds _period = 100ms;
  unsigned int _osc_port = 9001;
  test_engine = new engine(_name, _period, _osc_port);
  CPPUNIT_ASSERT(_name == test_engine->get_name());
  CPPUNIT_ASSERT(_period == test_engine->get_period());
  CPPUNIT_ASSERT(_osc_port == test_engine->get_osc_port());
}

void engineTest::testRunningThread()
{
  test_engine = new engine();
  CPPUNIT_ASSERT(test_engine->is_running());
}

void engineTest::testRestartingThread()
{
  test_engine = new engine();
  CPPUNIT_ASSERT(test_engine->is_running());
  test_engine->stop();
  CPPUNIT_ASSERT(!test_engine->is_running());
  test_engine->start();
  CPPUNIT_ASSERT(test_engine->is_running());
}

void engineTest::testAlreadyStartedThread()
{
  test_engine = new engine();
  CPPUNIT_ASSERT(test_engine->is_running());
  test_engine->start();
  CPPUNIT_ASSERT(test_engine->is_running());
}

void engineTest::testStopThread()
{
  test_engine = new engine();
  test_engine->stop();
  CPPUNIT_ASSERT(!test_engine->is_running());
}

void engineTest::testConflictedNameEngine()
{
  std::string _name = "testConflictedNameEngine";
  test_engine = new engine(_name);
  CPPUNIT_ASSERT_THROW(new engine(_name),
                       std::invalid_argument);
}

void engineTest::testInvalidNameEngine()
{
  CPPUNIT_ASSERT_THROW(new engine(""),
                       std::invalid_argument);
}

void engineTest::testConflictedOSCPortEngine()
{
  std::string _name = "testConflictedOSCPortEngine";
  std::string _other_name = "testConflictedOSCPortEngine_otherName";
  test_engine = new engine(_name);
  CPPUNIT_ASSERT_THROW(new engine(_other_name),
                       std::invalid_argument);
}

void engineTest::testGetLoopManager()
{
  test_engine = new engine("testGetLoopManager");
  CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(0),
                       test_engine->get_loop_manager()->get_loops()->size());
}

void engineTest::testRemoteStart()
{
  test_engine = new engine("testEngine", standard_engine_period, standard_osc_port);
  test_engine->stop();
  CPPUNIT_ASSERT(!test_engine->is_running());
  lo::Address address("localhost", standard_osc_port);
  address.send("/testEngine/start", "T");
  std::this_thread::sleep_for(standard_wait_time);
  CPPUNIT_ASSERT(test_engine->is_running());
  test_engine->stop();
  CPPUNIT_ASSERT(!test_engine->is_running());
  address.send("/testEngine/stop", "F");
  std::this_thread::sleep_for(standard_wait_time);
  CPPUNIT_ASSERT(test_engine->is_running());
}

void engineTest::testRemoteStop()
{
  test_engine = new engine("testEngine", standard_engine_period, standard_osc_port);
  CPPUNIT_ASSERT(test_engine->is_running());
  lo::Address address("localhost", standard_osc_port);
  address.send("/testEngine/stop", "T");
  std::this_thread::sleep_for(standard_wait_time);
  CPPUNIT_ASSERT(!test_engine->is_running());
  test_engine->start();
  CPPUNIT_ASSERT(test_engine->is_running());
  address.send("/testEngine/start", "F");
  std::this_thread::sleep_for(standard_wait_time);
  CPPUNIT_ASSERT(!test_engine->is_running());
}

void engineTest::testRemotePing()
{
  test_engine = new engine("PingPongEngine", standard_engine_period, standard_osc_port);
  CPPUNIT_ASSERT(test_engine->is_running());
  lo::ServerThread pong_server("9876");
  CPPUNIT_ASSERT(pong_server.is_valid());
  /*
   * Add a method handler for "/pong,ssi" using a C++11 lambda to keep
   * it succinct.  We capture a reference to the `received' count and
   * modify it atomically.
   */
  std::atomic<int> received(0);
  pong_server.add_method("/pong", "ssi",
                         [&received](lo_arg **, int)
  {
    ++received;
  });
  pong_server.start();
  CPPUNIT_ASSERT_EQUAL(0, static_cast<int>(received));
  lo::Address address("localhost", standard_osc_port);
  // Test with an explicit OSC target URL
  address.send("/PingPongEngine/ping", "ss", "osc.udp://localhost:9876", "/pong");
  std::this_thread::sleep_for(standard_wait_time);
  CPPUNIT_ASSERT_EQUAL(1, static_cast<int>(received));
  // Test with an implicit OSC target URL
  address.send("/PingPongEngine/ping", "ss", "localhost:9876", "/pong");
  std::this_thread::sleep_for(standard_wait_time);
  CPPUNIT_ASSERT_EQUAL(2, static_cast<int>(received));
  // Test with an unhandled argument list
  address.send("/PingPongEngine/ping", "ssTF", "localhost:9876", "/pong");
  std::this_thread::sleep_for(standard_wait_time);
  CPPUNIT_ASSERT_EQUAL(2, static_cast<int>(received));
}

void engineTest::testStressRemotePing()
{
  test_engine = new engine("PingPongEngine", standard_engine_period, standard_osc_port);
  CPPUNIT_ASSERT(test_engine->is_running());
  lo::ServerThread pong_server("9876");
  CPPUNIT_ASSERT(pong_server.is_valid());
  /*
   * Add a method handler for "/pong,ssi" using a C++11 lambda to keep
   * it succinct.  We capture a reference to the `received' count and
   * modify it atomically.
   */
  std::atomic<int> received(0);
  pong_server.add_method("/pong", "ssi",
                         [&received](lo_arg **, int)
  {
    ++received;
  });
  pong_server.start();
  CPPUNIT_ASSERT_EQUAL(0, static_cast<int>(received));
  lo::Address address("localhost", standard_osc_port);
  int iter_num = 1000;

  for(int i = 0; i < iter_num; ++i)
  {
    address.send("/PingPongEngine/ping", "ss", "osc.udp://localhost:9876", "/pong");
    std::this_thread::sleep_for(1ms);
  }

  CPPUNIT_ASSERT_EQUAL(iter_num, static_cast<int>(received));
}

void engineTest::test_set_name()
{
  test_engine = new engine("test_set_name_engine", standard_engine_period, standard_osc_port);
  CPPUNIT_ASSERT(test_engine->is_running());
  lo::ServerThread osc_control_server("9876");
  CPPUNIT_ASSERT(osc_control_server.is_valid());
  /*
   * Add a method handler for "/pong,ssi" using a C++11 lambda to keep
   * it succinct.  We capture a reference to the `received' count and
   * modify it atomically.
   */
  std::atomic<int> received(0);
  osc_control_server.add_method("/pong", "ssi",
                         [&received](lo_arg **, int)
  {
    ++received;
  });
  osc_control_server.start();
  CPPUNIT_ASSERT_EQUAL(0, static_cast<int>(received));
  lo::Address address("localhost", standard_osc_port);
  // Test with an explicit OSC target URL
  address.send("/test_set_name_engine/ping", "ss", "osc.udp://localhost:9876", "/pong");
  std::this_thread::sleep_for(standard_wait_time);
  CPPUNIT_ASSERT_EQUAL(1, static_cast<int>(received));
  test_engine->set_name("new_name");
  CPPUNIT_ASSERT(test_engine->is_running());
  address.send("/test_set_name_engine/ping", "ss", "osc.udp://localhost:9876", "/pong");
  std::this_thread::sleep_for(standard_wait_time);
  CPPUNIT_ASSERT_EQUAL(1, static_cast<int>(received));
  address.send("/new_name/ping", "ss", "osc.udp://localhost:9876", "/pong");
  std::this_thread::sleep_for(standard_wait_time);
  CPPUNIT_ASSERT_EQUAL(2, static_cast<int>(received));
}

#include "test_common.hpp"
