#include <cppunit/CompilerOutputter.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestResultCollector.h>
#include <cppunit/BriefTestProgressListener.h>

#include <log4cxx/logger.h>
#include <log4cxx/logmanager.h>
#include <log4cxx/basicconfigurator.h>

#include <string>

static log4cxx::LoggerPtr logger(log4cxx::Logger::getLogger("osclooper"));

int main(int argc, char **argv)
{
  logger->setLevel(log4cxx::Level::getOff());

  if(argc == 2)
  {
    std::string arg = argv[1];

    if(arg == "trace")
    {
      logger->setLevel(log4cxx::Level::getTrace());
    }
    else if(arg == "debug")
    {
      logger->setLevel(log4cxx::Level::getDebug());
    }
  }

  // Create the event manager and test controller
  CppUnit::TestResult controller;
  // Add a listener that colllects test result
  CppUnit::TestResultCollector result;
  controller.addListener(&result);
  // Add a listener that print brief line as test run.
  CppUnit::BriefTestProgressListener progress;
  controller.addListener(&progress);
  // Get the top level suite from the registry
  CppUnit::TestFactoryRegistry &registry = CppUnit::TestFactoryRegistry::getRegistry();
  CppUnit::Test *suite = registry.makeTest();
  // Adds the test to the list of test to run
  CppUnit::TextUi::TestRunner runner;
  runner.addTest(suite);
  // Change the default outputter to a compiler error format outputter
  runner.setOutputter(new CppUnit::CompilerOutputter(&runner.result(),
                      std::cerr));
  // Run the tests.
  runner.run(controller);
  // Return error code 1 if the one of test failed.
  return result.wasSuccessful() ? 0 : 1;
}
